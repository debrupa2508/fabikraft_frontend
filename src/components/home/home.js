import React from "react";

const Home = () => {
  return (
    <div>
      <header className="header_main">
        <div className="header_top">
          <div className="container">
            <div className="header_top_inner d-flex">
              <p>FREE SHIPPING ON ALL ORDERS OVER $75! START SHOPPING TODAY</p>
              <a href="/" className="gift_top">
                <i className="fas fa-gift" />
                {/*<img src={require("../../assets/images/gift_icon.png" class="img-responsive">*/}
                gift cards
              </a>
            </div>
          </div>
        </div>
        <div className="header_mid">
          <div className="container">
            <div className="header_mid_body d-flex">
              <div className="left_logo_area">
                <a href="/" className="logo">
                  <img
                    src={require("../../assets/images/logo.png").default}
                    className="img-responsive"
                    alt="fabikraft_image"
                  />
                </a>
                <form
                  action
                  method="post"
                  className="header_search"
                  id="form_search"
                >
                  <input
                    type="text"
                    className="head_search_input"
                    placeholder="Enter your keywords"
                  />
                  <button
                    type="submit"
                    id="btn_search"
                    className="head_search_btn"
                  >
                    <img
                      src={
                        require("../../assets/images/search_icon.png").default
                      }
                      alt="fabikraft_image"
                    />
                  </button>
                </form>
              </div>
              <div className="header_icons_area">
                <ul className="open_acnt">
                  <li>
                    <a href="/">Login</a>
                  </li>
                  |
                  <li>
                    <a href="/">register</a>
                  </li>
                </ul>
                <ul className="uer_cart">
                  <li>
                    <a href="/">
                      <img
                        src={require("../../assets/images/user.png").default}
                        className="userimg"
                        alt="fabikraft_image"
                      />
                    </a>
                  </li>
                  <li>
                    <a href="/">
                      <img
                        src={require("../../assets/images/cart.png").default}
                        className="userimg"
                        alt="fabikraft_image"
                      />
                    </a>
                  </li>
                </ul>
              </div>
              {/*added*/}
              <div className="mobile_searchbar_block">
                <div className="menu_bar">
                  <span className="menubar1" />
                  <span className="menubar2" />
                  <span className="menubar3" />
                </div>
                <form
                  action
                  method="post"
                  className="header_search"
                  id="form_search"
                >
                  <input
                    type="text"
                    className="head_search_input"
                    placeholder="Enter your keywords"
                  />
                  <button
                    type="submit"
                    id="btn_search"
                    className="head_search_btn"
                  >
                    <img
                      src={
                        require("../../assets/images/search_icon.png").default
                      }
                      alt="fabikraft_image"
                    />
                  </button>
                </form>
              </div>
              {/*end added*/}
            </div>
          </div>
        </div>
        <nav className="nav_bar_area" id="navbar_area">
          <div className="container">
            <div className="mobile_menu_head">
              <a href="/" className="close_menu">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  version={1.0}
                  width="16.000000pt"
                  height="16.000000pt"
                  viewBox="0 0 16.000000 16.000000"
                  preserveAspectRatio="xMidYMid meet"
                >
                  <g
                    transform="translate(0.000000,16.000000) scale(0.100000,-0.100000)"
                    fill="#000000"
                    stroke="none"
                  >
                    <path d="M0 153 c0 -4 14 -22 32 -40 l32 -33 -34 -35 c-44 -45 -30 -59 15 -15 l35 34 35 -34 c45 -44 59 -30 15 15 l-34 35 34 35 c44 45 30 59 -15 15 l-35 -34 -33 32 c-32 31 -47 39 -47 25z" />
                  </g>
                </svg>
              </a>
            </div>
            <ul className="menu_list_area">
              <li className="menu_text">
                <a href="/">
                  shop products
                  <i className="fas fa-chevron-down" />
                </a>
                <div className="dropdown_block animatedfast animatedFadeInUp fadeInUpFast">
                  <ul className="sub_menu_list">
                    <h6 className="subcatagory_menu">
                      <a href="/">Topwear</a>
                    </h6>
                    <li>
                      <a href="/">T-Shirts</a>
                    </li>
                    <li>
                      <a href="/">Casual Shirts</a>
                    </li>
                    <li>
                      <a href="/">Formal Shirts</a>
                    </li>
                    <li>
                      <a href="/">Sweatshirts</a>
                    </li>
                    <li>
                      <a href="/">Sweaters</a>
                    </li>
                    <li>
                      <a href="/">Jackets</a>
                    </li>
                    <li>
                      <a href="/">Blazers &amp; Coats</a>
                    </li>
                    <li>
                      <a href="/">Suits</a>
                    </li>
                    <li>
                      <a href="/">Jackets</a>
                    </li>
                    <div className="menu_seperator" />
                    <h6 className="subcatagory_menu">
                      <a href="/">Ethnic wear</a>
                    </h6>
                    <li>
                      <a href="/">Kurtas &amp; Kurta Sets</a>
                    </li>
                    <li>
                      <a href="/">Sherwanis</a>
                    </li>
                    <li>
                      <a href="/">Nehru Jackets</a>
                    </li>
                    <li>
                      <a href="/">Dhotis</a>
                    </li>
                  </ul>
                  <ul className="sub_menu_list">
                    <h6 className="subcatagory_menu">
                      <a href="/">Bottomwear</a>
                    </h6>
                    <li>
                      <a href="/">T-Shirts</a>
                    </li>
                    <li>
                      <a href="/">Casual Shirts</a>
                    </li>
                    <li>
                      <a href="/">Formal Shirts</a>
                    </li>
                    <li>
                      <a href="/">Sweatshirts</a>
                    </li>
                    <li>
                      <a href="/">Sweaters</a>
                    </li>
                    <div className="menu_seperator" />
                    <h6 className="subcatagory_menu">
                      <a href="/">Festivewear</a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/">Sleepwear</a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/">Innerwear</a>
                    </h6>
                  </ul>
                  <ul className="sub_menu_list">
                    <h6 className="subcatagory_menu">
                      <a href="/">Sports &amp; Activewear</a>
                    </h6>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Sports Shoes
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Sports Sandals
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Active T-Shirts
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Track Pants &amp; Shorts
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Tracksuits
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Jackets &amp; Sweatshirts
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Sports Accessories
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Swimwear
                      </a>
                    </li>
                    <div className="menu_seperator" />
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        personal care &amp; grroming
                      </a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Sunglasses
                      </a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        gadgets
                      </a>
                    </h6>
                  </ul>
                  <ul className="sub_menu_list">
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Fashion accessories
                      </a>
                    </h6>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Wallets
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Belts
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Perfumes &amp; Body Mists
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Trimmers
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Deodorants
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Ties, Cufflinks &amp; Pocket Squares
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Accessory Gift Sets
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Caps &amp; Hats
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Mufflers, Scarves &amp; Gloves
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Phone Cases
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Rings &amp; Wristwear
                      </a>
                    </li>
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Helmets
                      </a>
                    </li>
                  </ul>
                  <ul className="sub_menu_list">
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Footwear
                      </a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        watches
                      </a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Bags &amp; Backpacks
                      </a>
                    </h6>
                    <h6 className="subcatagory_menu">
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        Luggages &amp; Trolleys
                      </a>
                    </h6>
                  </ul>
                </div>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  brands
                  <i className="fas fa-chevron-down" />
                </a>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  sale
                </a>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  buy again
                </a>
              </li>
              <li className="menu_text active">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  deals
                </a>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  most popular
                </a>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  rewards
                </a>
              </li>
              <li className="menu_text">
                <a href="/" onClick={(event) => event.preventDefault()}>
                  subscription
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <section className="banner_main">
        <div
          id="carouselControls"
          className="carousel slide"
          data-ride="carousel"
        >
          <ol className="carousel-indicators">
            <li
              data-target="#carouselControls"
              data-slide-to={0}
              className="active"
            />
            <li data-target="#carouselControls" data-slide-to={1} className />
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="banner_row">
                <img
                  src={require("../../assets/images/banner_1.jpg").default}
                  alt="fabikraft_image"
                />
                <div className="banner_text_area">
                  <div className="container">
                    <div className="banner_text animated animatedFadeInUp fadeInUp">
                      <h4 className="ds_text">Super Saver Days</h4>
                      <h5>handicrafts of india</h5>
                      <h3>that you must buy</h3>
                      <h6>from $199</h6>
                      <a
                        href="/"
                        onClick={(event) => event.preventDefault()}
                        className="banner_btn"
                      >
                        <span>shop now</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <div className="banner_row">
                <img
                  src={require("../../assets/images/bn2.jpg").default}
                  alt="fabikraft_image"
                />
                <div className="banner_text_area">
                  <div className="container">
                    <div className="banner_text animated animatedFadeInUp fadeInUp">
                      <h4 className="ds_text">Super Saver Days</h4>
                      <h5>handicrafts of india</h5>
                      <h3>that you must buy</h3>
                      <h6>from $199</h6>
                      <a
                        href="/"
                        onClick={(event) => event.preventDefault()}
                        className="banner_btn"
                      >
                        <span>shop now</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <a
            className="sliding_left"
            href="#carouselControls"
            role="button"
            data-slide="prev"
          >
            <i className="fas fa-chevron-left" />
          </a>
          <a
            className="sliding_right"
            href="#carouselControls"
            role="button"
            data-slide="next"
          >
            <i className="fas fa-chevron-right" />
          </a>
        </div>
      </section>
      <section className="shopbycategories">
        <div className="container">
          <h3 className="block_heading">
            <div className="block_heading_dash">+</div>
            <span>shop by category</span>
            <div className="block_heading_dash">+</div>
          </h3>
          <div className="category_area">
            <div className="category_slider">
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/gift_card.png").default}
                  alt="fabikraft_image"
                />
                gift card
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/kurtas.png").default}
                  alt="fabikraft_image"
                />
                kurtas
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/jwellery.png").default}
                  alt="fabikraft_image"
                />
                jwellery
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list active"
              >
                <img
                  src={require("../../assets/images/saree.png").default}
                  alt="fabikraft_image"
                />
                sarees
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/dupatta.png").default}
                  alt="fabikraft_image"
                />
                dupattas
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/handbag.png").default}
                  alt="fabikraft_image"
                />
                bags
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/shoe.png").default}
                  alt="fabikraft_image"
                />
                footwear
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/decor.png").default}
                  alt="fabikraft_image"
                />
                decor
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/linen.png").default}
                  alt="fabikraft_image"
                />
                home linen
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/jwellery.png").default}
                  alt="fabikraft_image"
                />
                jwellery
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/saree.png").default}
                  alt="fabikraft_image"
                />
                sarees
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/dupatta.png").default}
                  alt="fabikraft_image"
                />
                dupattas
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/handbag.png").default}
                  alt="fabikraft_image"
                />
                bags
              </a>
              <a
                href="/"
                onClick={(event) => event.preventDefault()}
                className="category_list"
              >
                <img
                  src={require("../../assets/images/shoe.png").default}
                  alt="fabikraft_image"
                />
                footwear
              </a>
            </div>
          </div>
          {/*<div class="shop_category">
				<div class="row shop_category_row">
					<div class="col-lg-7 col-md-7 col-sm-12">
						<div class="row">
							<div class="col-6 category_toshop">
								<img src={require("../../assets/images/category1.jpg").default}>
							</div>
							<div class="col-6 category_toshop">
								<img src={require("../../assets/images/category2.jpg").default}>
							</div>
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-12">
						<div class="row shop_category_row">
							<div class="col-12 category_toshop showpiece">
								<img src={require("../../assets/images/category3.jpg").default}>
							</div>
							<div class="col-12 category_toshop bags_img">
								<img src={require("../../assets/images/category4.jpg").default} class="">
							</div>
						</div>
					</div>
				</div>
			</div>*/}
          <div className="shop_category">
            <div className="shop_category_row">
              <div className="shop_category_rowleft">
                <div className="category_toshop">
                  <img
                    src={require("../../assets/images/category1.jpg").default}
                    alt="fabikraft_image"
                  />
                  <div className="text_over_category animatedfast animatedFadeInUp fadeInUp">
                    <p className="sm_text_ds">This is a</p>
                    <h4>simple headline</h4>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
                <div className="category_toshop">
                  <img
                    src={require("../../assets/images/category2.jpg").default}
                    alt="fabikraft_image"
                  />
                  <div className="text_over_category animatedfast animatedFadeInUp fadeInUp">
                    <p className="sm_text_ds">This is a</p>
                    <h4>simple headline</h4>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="shop_category_rowright">
                <div className="category_toshop">
                  <img
                    src={require("../../assets/images/category3.jpg").default}
                    alt="fabikraft_image"
                  />
                  <div className="text_over_category animatedfast animatedFadeInUp fadeInUp">
                    <p className="sm_text_ds">This is a</p>
                    <h4>simple headline</h4>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
                <div className="category_toshop">
                  <img
                    src={require("../../assets/images/3.jpg").default}
                    alt="fabikraft_image"
                  />
                  <div className="text_over_category animatedfast animatedFadeInUp fadeInUp">
                    <p className="sm_text_ds">This is a</p>
                    <h4>simple headline</h4>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="deals_main_sec">
        <div className="container">
          <h3 className="block_heading">
            <div className="block_heading_dash">+</div>
            <span>Today's Fashion Deals</span>
            <div className="block_heading_dash">+</div>
          </h3>
          <div className="deals_row">
            <div className="deals_head_area">
              <h3>deals of the day</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
                commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus
                mus. Donec quam feli
              </p>
            </div>
            <div className="d-flex">
              <div className="deals_product">
                <img
                  src={require("../../assets/images/fashion1.jpg").default}
                  alt="fabikraft_image"
                />
                <div className="deals_overlay">
                  <h6 className="product_name">onsectetuer adipi</h6>
                  <p>$200</p>
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="shop_btn_sm"
                  >
                    shop now
                  </a>
                </div>
              </div>
              <div className="deals_product">
                <img
                  src={require("../../assets/images/fashion2.jpg").default}
                  alt="fabikraft_image"
                />
                <div className="deals_overlay">
                  <h6 className="product_name">onsectetuer adipi</h6>
                  <p>$200</p>
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="shop_btn_sm"
                  >
                    shop now
                  </a>
                </div>
              </div>
              <div className="deals_product">
                <img
                  src={require("../../assets/images/fashion3.jpg").default}
                  alt="fabikraft_image"
                />
                <div className="deals_overlay">
                  <h6 className="product_name">
                    onsectetuer adipi onsectetuer adipi onsectetuer adipi
                  </h6>
                  <p className="prc">$200</p>
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="shop_btn_sm"
                  >
                    shop now
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="deals_main_sec pb-0">
        <div className="container">
          <h3 className="block_heading">
            <div className="block_heading_dash popular_head_dash">+</div>
            <span>Most Popular</span>
            <div className="block_heading_dash">+</div>
          </h3>
          <div className="popular_toshow">
            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr1.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr2.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">
                        onsectetuer adipi onsectetuer adipi onsectetuer adipi
                      </h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: 4 }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr3.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "4.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr4.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr5.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/popular3.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/popular4.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div className="popular_box">
                  <a
                    href="/"
                    onClick={(event) => event.preventDefault()}
                    className="popular_imgbox"
                  >
                    <img
                      src={require("../../assets/images/pr6.jpg").default}
                      alt="fabikraft_product"
                      className="img-responsive"
                    />
                  </a>
                  <div className="popular_desc">
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="popular_imgbox"
                    >
                      <h6 className="product_name">onsectetuer adipi</h6>
                      <div className="rating">
                        <div className="Stars" style={{ rating: "3.5" }} />
                      </div>
                      <p className="prc">$199</p>
                    </a>
                    <a
                      href="/"
                      onClick={(event) => event.preventDefault()}
                      className="shop_btn_sm"
                    >
                      <span>shop now</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="gift_section_wrap">
        <div className="container">
          <div className="gift_section">
            <div className="gift_logo_bg">
              <img
                src={require("../../assets/images/logo2.png").default}
                alt="fabikraft_logo"
              />
            </div>
            <div className="seal_imgbox">
              <img
                src={require("../../assets/images/sealed1.png").default}
                alt="fabikraft_seal"
              />
              <img
                src={require("../../assets/images/sealed2.png").default}
                alt="fabikraft_seal"
              />
              <img
                src={require("../../assets/images/sealed3.png").default}
                alt="fabikraft_seal"
              />
            </div>
            <div className="gift_text">
              <h5>fabikraft</h5>
              <h3>gift card</h3>
              <p>Sign up with us to get a 15% Gift Card Discount</p>
              <form
                action
                method="post"
                id="gift_signin_form"
                className="form_gift"
              >
                <input
                  type="email"
                  name="u_mail"
                  id="u_mail"
                  className="form-control"
                  placeholder="Your email address"
                />
                <button type="submit" id="btn_giftsignin">
                  sign up now
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section className="instagram_img_area">
        <div className="container">
          <h3 className="block_heading">
            <div className="block_heading_dash">+</div>
            <span>Follow us on Instagram</span>
            <div className="block_heading_dash">+</div>
          </h3>
        </div>
        <div className="instagram_img_container">
          <div className="insta_row d-flex">
            <a
              href="/"
              onClick={(event) => event.preventDefault()}
              className="insta_img_box"
            >
              <img
                src={require("../../assets/images/insta1.jpg").default}
                className="img-responsive"
                alt="fabikraft_image"
              />
            </a>
            <a
              href="/"
              onClick={(event) => event.preventDefault()}
              className="insta_img_box"
            >
              <img
                src={require("../../assets/images/insta2.jpg").default}
                className="img-responsive"
                alt="fabikraft_image"
              />
            </a>
            <a
              href="/"
              onClick={(event) => event.preventDefault()}
              className="insta_img_box"
            >
              <img
                src={require("../../assets/images/insta3.jpg").default}
                className="img-responsive"
                alt="fabikraft_image"
              />
            </a>
            <a
              href="/"
              onClick={(event) => event.preventDefault()}
              className="insta_img_box"
            >
              <img
                src={require("../../assets/images/insta4.jpg").default}
                className="img-responsive"
                alt="fabikraft_image"
              />
            </a>
            <a
              href="/"
              onClick={(event) => event.preventDefault()}
              className="insta_img_box"
            >
              <img
                src={require("../../assets/images/insta5.jpg").default}
                className="img-responsive"
                alt="fabikraft_image"
              />
            </a>
          </div>
        </div>
      </section>
      <section className="service_available">
        <div className="container">
          <div className="d-flex">
            <h5>
              <span className="facility_icon">
                <img
                  src={require("../../assets/images/shipping.png").default}
                  className="img-responsive"
                  alt="fabikraft_shipping"
                />
              </span>
              <span className="thefacility">quick shipping</span>
            </h5>
            <h5>
              <span className="facility_icon">
                <img
                  src={require("../../assets/images/return.png").default}
                  className="img-responsive"
                  alt="fabikraft_return"
                />
              </span>
              <span className="thefacility">returns and exchanges</span>
            </h5>
            <h5>
              <span className="facility_icon">
                <img
                  src={require("../../assets/images/secure.png").default}
                  className="img-responsive"
                  alt="fabikraft_secure"
                />
              </span>
              <span className="thefacility">secure payment</span>
            </h5>
          </div>
        </div>
      </section>
      <footer className="ft_main">
        <div className="ft_top">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-3 col-sm-12">
                <div className="ft_about">
                  <h5 className="ft_heading">
                    <img
                      src={require("../../assets/images/ft_logo.png").default}
                      alt="fabikraft_logo"
                      className="ft_logo img-responsive"
                    />
                    who we are
                  </h5>
                  <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                    natoque penatibus et magnis dis parturient montes, nascetur
                    ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                    eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede{" "}
                  </p>
                </div>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-12">
                <div className="ft_menu_area d-flex">
                  <div className="ft_menu_col">
                    <h5 className="ft_heading">important links</h5>
                    <ul className="ft_menu_list">
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          About Fabikraft
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Help
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Terms &amp; Conditions
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Return Policies
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Shipping
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Contact us
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Privacy
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Careers
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="ft_menu_col">
                    <h5 className="ft_heading">products</h5>
                    <ul className="ft_menu_list">
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Lorem ipsum dolor sit
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Consectetuer
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Adipiscing elit.
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Ommodo{" "}
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Eget dolor.
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Aenean
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Cum sociis natoque
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          <b>All Products</b>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="ft_menu_col">
                    <h5 className="ft_heading">brands</h5>
                    <ul className="ft_menu_list">
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Lorem ipsum dolor sit
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Consectetuer
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Adipiscing elit.
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Ommodo{" "}
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Eget dolor.
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Aenean
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          Cum sociis natoque
                        </a>
                      </li>
                      <li>
                        <a href="/" onClick={(event) => event.preventDefault()}>
                          <b>All Brands</b>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-12">
                <div className="ft_contact">
                  <h5 className="ft_heading">get in touch</h5>
                  <ul className="ft_menu_list">
                    <li>
                      <a href="/" onClick={(event) => event.preventDefault()}>
                        <span className="contact_icon">
                          <img
                            src={require("../../assets/images/map.png").default}
                            alt="fabikraft_map"
                          />
                        </span>
                        <span className="ft_address">
                          91 Springboard BKC, Mumbai , India !
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="tel:9820250350">
                        <span className="contact_icon">
                          <img
                            src={
                              require("../../assets/images/phone.png").default
                            }
                            alt="fabikraft_phone"
                          />
                        </span>
                        <span className="ft_address">
                          <small>+91&nbsp;9820250350</small> /{" "}
                          <small>+91&nbsp;9819606865</small>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="mailto:fab@fabikraft.com">
                        <span className="contact_icon">
                          <img
                            src={
                              require("../../assets/images/mail.png").default
                            }
                            alt="fabikraft_mail"
                          />
                        </span>
                        <span className="ft_address">fab@fabikraft.com</span>
                      </a>
                    </li>
                  </ul>
                  <h5 className="ft_heading diff">payment</h5>
                  <ul className="pay_mode d-flex">
                    <li>
                      <img
                        src={require("../../assets/images/visa.png").default}
                        className="img-responsive"
                        alt="fabikraft_visa"
                      />
                    </li>
                    <li>
                      <img
                        src={require("../../assets/images/mc.png").default}
                        className="img-responsive"
                        alt="fabikraft_mastercard"
                      />
                    </li>
                    <li>
                      <img
                        src={require("../../assets/images/paypal.png").default}
                        className="img-responsive"
                        alt="fabikraft_paypal"
                      />
                    </li>
                    <li>
                      <img
                        src={
                          require("../../assets/images/amricanex.png").default
                        }
                        className="img-responsive"
                        alt="fabikraft_americanex"
                      />
                    </li>
                    <li>
                      <img
                        src={require("../../assets/images/upi.png").default}
                        className="img-responsive"
                        alt="fabikraft_upi"
                      />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="ft_bottom">
          <div className="container">
            <ul className="social_media_list">
              <li>
                <a
                  href="/"
                  onClick={(event) => event.preventDefault()}
                  target="_blank"
                >
                  <i className="fab fa-facebook-f" />
                </a>
              </li>
              <li>
                <a
                  href="/"
                  onClick={(event) => event.preventDefault()}
                  target="_blank"
                >
                  <i className="fab fa-twitter" />
                </a>
              </li>
              <li>
                <a
                  href="/"
                  onClick={(event) => event.preventDefault()}
                  target="_blank"
                >
                  <i className="fab fa-google-plus-g" />
                </a>
              </li>
              <li>
                <a
                  href="/"
                  onClick={(event) => event.preventDefault()}
                  target="_blank"
                >
                  <i className="fab fa-linkedin-in" />
                </a>
              </li>
              <li>
                <a
                  href="/"
                  onClick={(event) => event.preventDefault()}
                  target="_blank"
                >
                  <i className="fab fa-youtube" />
                </a>
              </li>
            </ul>
            © Copyright 2021 FABIKRAFT - All Rights Reserved
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Home;
